const express = require('express');
const app = express();
const port = process.env.PORT || 8080;

app.use('', (req: any, res: any, next: any) => {
    console.log('URL:', req.url);
    next();
});

app.get('/', (req: any, res: any) => {
    res.send('Hello BookDatabase (ts app)!');
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
