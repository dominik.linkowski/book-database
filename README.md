# Book database

**VCS:** [https://gitlab.com/dominik.linkowski/book-database](https://gitlab.com/dominik.linkowski/book-database)

**Docker Hub**
- **SERVER:** [https://hub.docker.com/repository/docker/dominiklinkowski/book-database-server](https://hub.docker.com/repository/docker/dominiklinkowski/book-database-server)
- **DB:** [https://hub.docker.com/repository/docker/dominiklinkowski/book-database-db](https://hub.docker.com/repository/docker/dominiklinkowski/book-database-db)
- **VIEW:** [https://hub.docker.com/repository/docker/dominiklinkowski/book-database-view](https://hub.docker.com/repository/docker/dominiklinkowski/book-database-view)

- SERVER works on port: `8080:8080`
- DB works on port: `3307:3306`
- VIEW works on port: `4200:80`

## How to run

**Required:**
- docker

**Steps:**
1. clone project
2. `docker-compose build`
3. `docker-compose up`

## Authors:

- [Dominik Linkowski](https://github.com/linkowski)
- Jakub Szymonek



## Project description:

Database application with an interface in a browser-based on the Angular framework in version 10.x



## How to use

- [x] Description of how to start the application
- [x] Required dependencies for commissioning



## Functionalities:

- [ ] Adding records
- [ ] Viewing records
- [ ] Modification of the selected record
- [ ] Delete selected record
- [ ] Adding a new record
- [ ] Sorting the displayed information
- [ ] Counting the time spent in the program in the form of HH:MM:SS after the program ends
- [ ] Viewing visitors



## Used technologies:

- nodejs with typescript
- typeorm
- jestjs (backend tests)
- Angular 10.x (frontend)
- Mysql
- docker + docker compose
- Gitlab CI/CD



## Models:

Presentation of models.



### BookBase

```typescript
interface BookBase {
  books: Book[];
  visitors: Visitor[];
}
```



### Book

```typescript
interface Book {
  title: string;
  author: string;
  publisher: string;
  release: Date;
  ISBN: string;
}
```



### ISBN

```typescript
const ISBN = /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;
```



### Visitor

```typescript
interface Visitor {
  ip: string;
  date: Date;
  spentTime: number;
}
```



### IP

```typescript
const IP = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;    
```
